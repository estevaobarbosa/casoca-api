from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from .models import marketplace_userdata

# #############
# # Lojas: marketplace_userdata
#
class LojasList(LoginRequiredMixin, ListView):
    model = marketplace_userdata
    template_name = 'magento/lista_lojas.html'

