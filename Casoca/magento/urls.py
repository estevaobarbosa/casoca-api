#Core Django imports
from django.conf.urls import url

from .views import (
    LojasList,
)

urlpatterns = [
    # Lojas
    url(r'^lojas/$',LojasList.as_view(),name='lista-lojas'),
]