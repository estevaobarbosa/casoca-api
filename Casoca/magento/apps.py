from django.apps import AppConfig


class MagentoConfig(AppConfig):
    name = 'Casoca.magento'
