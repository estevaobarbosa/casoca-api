from django.db import models

class customer_entity(models.Model):

    entity_id = models.PositiveIntegerField('Id da Loja',primary_key=True)
    website_id = models.PositiveSmallIntegerField('Website Id')
    email = models.CharField('Email', max_length = 255)
    group_id = models.PositiveSmallIntegerField('Group Id')
    increment_id = models.CharField('Increment Id', max_length = 50)
    store_id = models.PositiveSmallIntegerField('Store Id')
    created_at = models.DateTimeField("Created At")
    updated_at = models.DateTimeField("Updated At")
    is_active = models.PositiveSmallIntegerField('Is Active')
    disable_auto_group_change = models.PositiveSmallIntegerField('Disable automatic group change based on VAT ID')
    created_in = models.CharField('Created From', max_length = 255)
    prefix = models.CharField('Prefix', max_length = 40)
    firstname = models.CharField('First Name', max_length = 255)
    middlename = models.CharField('Middle Name/Initial', max_length = 255)
    lastname = models.CharField('Last Name', max_length = 255)
    suffix = models.CharField('Suffix', max_length = 40)
    dob = models.DateField("Date of Birth")
    password_hash = models.CharField('Password_hash', max_length = 128)
    rp_token = models.CharField('Reset password token', max_length = 128)
    rp_token_created_at = models.DateTimeField("Reset password token creation time")
    default_billing = models.PositiveIntegerField('Default Billing Address')
    default_shipping = models.PositiveIntegerField('Default Shipping Address')
    taxvat = models.CharField('Tax/VAT Number', max_length = 50)
    confirmation = models.CharField('Is Confirmed', max_length = 64)
    gender = models.PositiveSmallIntegerField('Gender')
    failures_num = models.SmallIntegerField('Failure Number')
    first_failure = models.DateTimeField("First Failure")
    lock_expires = models.DateTimeField("Lock Expiration Date")

    class Meta:
        db_table = 'customer_entity'
        app_label = 'magento'


class marketplace_userdata(models.Model):

    entity_id = models.PositiveIntegerField('Id da Loja',primary_key=True)
    shop_title = models.TextField('Shop Title')
    company_description = models.TextField('Descrição')

    class Meta:
        db_table = 'marketplace_userdata'
        app_label = 'magento'
        ordering = ['shop_title']

    def __str__(self):
        return self.shop_title