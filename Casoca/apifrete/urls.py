from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from Casoca.apifrete import views

urlpatterns = format_suffix_patterns([
    url(r'^$', views.api_root),

    url(r'^calculafrete/$',views.CalculaValorFrete.as_view(), name='valorfrete-create'),
    url(r'^valorfrete/(?P<pk>[0-9]+)/$', views.DetailValorFrete.as_view(), name='valorfrete-detail'),
])