from django.db import models
from Casoca.principal.choices import SIM_NAO

class ValorFrete(models.Model):

    loja = models.IntegerField()
    identificador = models.CharField(max_length=5)
    cep = models.IntegerField()
    produto = models.IntegerField()
    quantidade = models.IntegerField()
    taxa_agregacao = models.FloatField()
    tempo_postagem = models.IntegerField()
    peso = models.FloatField()
    altura = models.FloatField()
    largura = models.FloatField()
    profundidade = models.FloatField()

    valor = models.DecimalField(decimal_places=2, max_digits=10)
    tempo_entrega = models.IntegerField()
    calculo_ok = models.CharField('Cálculo Ok', max_length=1, choices=SIM_NAO, default='S', blank=True, null=True)
    mensagem = models.CharField('Mensagem', max_length=60, default='Ok')

    criado = models.DateTimeField("Criado em", auto_now_add=True)
    modificado = models.DateTimeField("Modificado em", auto_now=True)
