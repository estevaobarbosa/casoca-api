from rest_framework import serializers
from .models import ValorFrete
from Casoca.tabelas.models import Transportadoras, Fretes

class ValorFreteSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = ValorFrete
        fields = ('__all__')

    def save(self, **kwargs):
        print('save')
        self.validated_data['identificador'] = 'Novo'
        return super().save(**kwargs)

