#!/usr/bin/python
# -*- coding: UTF-8 -*-

from rest_framework import status, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from .models import ValorFrete
from .serializers import ValorFreteSerializer
from .functions.calculafrete import Frete, CalculaFrete

from Casoca.tabelas.models import Transportadoras, Fretes

class CalculaValorFrete(generics.CreateAPIView):
    serializer_class = ValorFreteSerializer

    def post(self, request, *args, **kwargs):

        frete = Frete()
        frete.loja = request.POST.get('loja')
        frete.transportadora = 0
        frete.identificador = request.POST.get('identificador')
        frete.cep = request.POST.get('cep')
        frete.produto = request.POST.get('produto')
        frete.quantidade = request.POST.get('quantidade')
        frete.taxa_agregacao = request.POST.get('taxa_agregacao')
        frete.tempo_postagem = request.POST.get('tempo_postagem')
        frete.peso = request.POST.get('peso')
        frete.altura = request.POST.get('altura')
        frete.largura = request.POST.get('largura')
        frete.profundidade = request.POST.get('profundidade')
        frete.valor = request.POST.get('valor')
        frete.tempo_entrega = request.POST.get('tempo_entrega')
        frete.calculo_ok = 'S'
        frete.mensagem = ''

        calculaFrete = CalculaFrete()
        fretecalculado = calculaFrete.calculo(frete)


        return super().post(request, *args, **kwargs)


class DetailValorFrete(generics.RetrieveUpdateAPIView):
    queryset = ValorFrete.objects.all()
    serializer_class = ValorFreteSerializer

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'Cálcula Valor do Frete': reverse('valorfrete-create', request=request, format=format),
    })
