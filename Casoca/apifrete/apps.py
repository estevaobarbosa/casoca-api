from django.apps import AppConfig


class ApifreteConfig(AppConfig):
    name = 'Casoca.apifrete'
