from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


def home(request):
    return render(request, 'home.html')


def loginview(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'login.html', {'mensagem': 'Usuário/senha inválidos'})
    else:
        return render(request, 'login.html')

@login_required
def logoutview(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
