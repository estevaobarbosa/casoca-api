from django.apps import AppConfig


class PrincipalConfig(AppConfig):
    name = 'Casoca.principal'
