from django.conf.urls import url, include
from django.contrib import admin
from Casoca.principal import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^login', views.loginview, name='login'),
    url(r'^logout', views.logoutview, name='logout'),
    url(r'^admin/', admin.site.urls),

    url(r'^apifrete/', include('Casoca.apifrete.urls')),
    url(r'^magento/', include('Casoca.magento.urls', namespace="magento")),
    url(r'^planilhas/', include('Casoca.excel.urls', namespace="planilhas")),
    url(r'^tabelas/',include('Casoca.tabelas.urls', namespace="tabelas")),
]
