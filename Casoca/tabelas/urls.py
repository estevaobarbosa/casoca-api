#Core Django imports
from django.conf.urls import url

from .views import (
    TransportadorasList, TransportadorasCreate, TransportadorasUpdate, TransportadorasDelete
)

urlpatterns = [
    # Transportadoras
    url(r'^listatransportadoras/$',TransportadorasList.as_view(),name='lista-transportadoras'),
    url(r'^novatransportadora/$',TransportadorasCreate.as_view(),name='nova-transportadora'),
    url(r'^editatransportadora/(?P<pk>\d+)/$',TransportadorasUpdate.as_view(),name='edita-transportadora'),
    url(r'^excluitransportadora/(?P<pk>\d+)/$', TransportadorasDelete.as_view(),name='exclui-transportadora'),

]