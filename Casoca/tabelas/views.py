from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from .forms import TransportadorasForm
from .models import Transportadoras, Fretes
#
# #############
# # Transportadoras
#
class TransportadorasList(LoginRequiredMixin, ListView):
    model = Transportadoras
    template_name = 'tabelas/lista_transportadoras.html'
    def get_queryset(self):

        try:
            empresas = Transportadoras.objects.order_by('nome_fantasia').all()
        except Transportadoras.DoesNotExist:
            empresas = Transportadoras()

        return empresas


class TransportadorasCreate(LoginRequiredMixin, CreateView):
    model = Transportadoras
    template_name = 'tabelas/form_padrao.html'
    form_class =  TransportadorasForm
    success_url = reverse_lazy('tabelas:lista-empresas')

    def get_context_data(self, **kwargs):
        data = super(TransportadorasCreate, self).get_context_data(**kwargs)
        data['tabela'] = 'Transportadoras'
        data['list_url'] = 'tabelas:lista-transportadoras'
        data['operacao'] = 'Incluir'
        return data


class TransportadorasUpdate(LoginRequiredMixin, UpdateView):
    model = Transportadoras
    template_name = 'tabelas/form_padrao.html'
    form_class =  TransportadorasForm
    success_url = reverse_lazy('tabelas:lista-transportadoras')

    def get_context_data(self, **kwargs):
        data = super(TransportadorasUpdate, self).get_context_data(**kwargs)
        data['tabela'] = 'Transportadoras'
        data['list_url'] = 'tabelas:lista-transportadoras'
        data['operacao'] = 'Salvar'
        return data


class TransportadorasDelete(LoginRequiredMixin, DeleteView):
    model = Transportadoras
    template_name = 'tabelas/confirma_exclusao.html'
    success_url = reverse_lazy('tabelas:lista-transportadoras')

    def get_context_data(self, **kwargs):
        data = super(TransportadorasDelete, self).get_context_data(**kwargs)
        data['tabela'] = 'Transportadoras'
        data['list_url'] = 'tabelas:lista-transportadoras'
        return data
