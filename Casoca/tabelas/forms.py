from django import forms

from localflavor.br.forms import BRCNPJField, BRPhoneNumberField, BRZipCodeField

from .models import Transportadoras, Fretes
from Casoca.magento.models import marketplace_userdata

class TransportadorasForm(forms.ModelForm):

    cnpj = BRCNPJField(label='CNPJ',
                       widget=forms.TextInput(
                           attrs={
                               'class': 'form-control cnpj',
                               'placeholder': '99.999.999/9999-99',
                               'title': 'CNPJ no formato 99.999.999/9999-99',
                           }
                       ),
                       min_length=15,
                       required=True)


    identificador = forms.CharField(label='Identificador da transportadora',
                                   widget=forms.TextInput(attrs={
                                       'class': 'form-control',
                                       'placeholder': 'XXXXX',
                                   }),
                                   help_text='Identificador da transportadora utilizado para associar aos produtos.',
                                   required=True,
                                   max_length=5
                                   )

    nome_fantasia = forms.CharField(label='Nome Fantasia',
                                   widget=forms.TextInput(attrs={
                                       'class': 'form-control',
                                       'placeholder': 'Nome Fantasia',
                                   }),
                                   required=True,
                                   max_length=60
                                   )

    razao_social = forms.CharField(label='Razão Social',
                                  widget=forms.TextInput(attrs={
                                      'class': 'form-control',
                                      'placeholder': 'Razão Social',
                                  }),
                                  required=True,
                                  max_length=100
                                  )

    endereco = forms.CharField(label='Endereço',
                               widget=forms.TextInput(attrs={
                                   'placeholder': 'Rua, avenida, praça ....',
                                   'class': 'form-control',
                               }),
                               required=True,
                               max_length=60
                               )

    numero = forms.IntegerField(label='Número',
                                     widget=forms.NumberInput(attrs={
                                         'class': 'form-control',
                                     }),
                                     required=True,
                                     )

    bairro = forms.CharField(label='Bairro',
                             widget=forms.TextInput(attrs={
                                 'class': 'form-control',
                             }),
                             required=True,
                             max_length=50
                             )

    cidade = forms.CharField(label='Cidade',
                             widget=forms.TextInput(attrs={
                                 'class': 'form-control',
                             }),
                             required=True,
                             initial='Belo Horizonte',
                             max_length=50
                             )

    telefone = BRPhoneNumberField(widget=forms.TextInput(attrs={
        'class': 'form-control phone',
        "placeholder": "(99)9999-9999",
        "title": "Telefone de contato no formato (99)99999-9999"}),
        required=False,
    )

    cep = BRZipCodeField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        "placeholder": "99999-999",
        "title": "CEP no formato 99999-999"}),
        max_length=9,
        required=True,
    )


    class Meta:
        model = Transportadoras
        fields = ['loja','cnpj', 'identificador', 'nome_fantasia', 'razao_social', 'codigo_contrato', 'inscricao_estadual', 'inscricao_minicipal',
                  'telefone', 'endereco', 'numero', 'complemento', 'bairro', 'cep', 'cidade', 'uf', 'contatos', 'habilitado']

        widgets = {

            "loja": forms.Select(
                attrs={
                    'class': 'form-control',
                },
            ),

            "codigo_contrato": forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Código do contrato junto à transportadora'
                },
            ),

            "inscricao_estadual": forms.TextInput(
                attrs={
                    'class': 'form-control',
                },
            ),

            "inscricao_minicipal": forms.TextInput(
                attrs={
                    'class': 'form-control',
                },
            ),

            'complemento': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Informações adicionais ao endereço',
                }
            ),
            'uf': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'contatos': forms.Textarea(
                attrs={
                    'class': 'form-control',
                }
            ),
            "habilitado": forms.Select(
                attrs={
                    'class': 'form-control',
                },
            ),
        }

        help_texts = {
            'cnpj': (
                'Cadastro Nacional da Pessoa Jurídica'
            ),
            'cep': (
                'Insira somente os números do cep.'
            ),
            'endereco': (
                'Rua, avenida, viela...'
            ),
            'numEndereco': (
                'Número do endereço.'
            ),
            'complemento': (
                'Complemento para este endereço.'
            ),
            'bairro': (
                'Bairro ou distrito.'
            ),
            'cidade': (
                'Cidade deste endereço.'
            ),
        }