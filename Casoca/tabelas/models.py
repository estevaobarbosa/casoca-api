from django.db import models
from localflavor.br.br_states import STATE_CHOICES

from Casoca.principal.choices import SIM_NAO

class Transportadoras(models.Model):

    cnpj = models.CharField('CNPJ', max_length=18, blank=True, null=True)
    identificador = models.CharField('Identificador', max_length=5)
    loja = models.ForeignKey('magento.marketplace_userdata',verbose_name='Loja', blank=True, null=True)
    nome_fantasia = models.CharField('Transportadora', max_length=60)
    razao_social = models.CharField('Razão Social', max_length=200, blank=True, null=True)
    codigo_contrato = models.CharField('Código do Contrato de Logística', max_length=60, blank=True, null=True)
    inscricao_estadual = models.CharField('Inscrição Estadual', max_length=25, blank=True, null=True)
    inscricao_minicipal = models.CharField('Inscrição Municipal', max_length=25, blank=True, null=True)
    endereco = models.CharField('Endereço', max_length=60)
    numero = models.PositiveSmallIntegerField('Número')
    complemento = models.CharField('Complemento',max_length=10, blank=True, null=True)
    bairro = models.CharField('Bairro',max_length=20)
    cep = models.CharField('CEP', max_length=9)
    cidade = models.CharField('Cidade',max_length=50)
    uf = models.CharField('Unidade da Federação', max_length=2, choices=STATE_CHOICES, default='MG')
    telefone = models.CharField('Telefone de Contato', max_length=15, blank=True, null=True)
    contatos = models.TextField("Informações de Contatos", blank=True, null=True)
    habilitado = models.CharField('Habilitado', max_length=1, choices=SIM_NAO, default='S', blank=True, null=True)
    criado = models.DateTimeField("Criado em", auto_now_add=True)
    modificado = models.DateTimeField("Modificado em", auto_now=True)

    class Meta:
        verbose_name = 'Transportadora'
        verbose_name_plural = 'Transportadoras'
        ordering = ['nome_fantasia', 'cidade']
        unique_together=[('loja','identificador'),]

    def __str__(self):
        return self.nome_fantasia + ' - ' + self.cidade + '-' + self.uf


class Fretes(models.Model):

    loja = models.ForeignKey('magento.marketplace_userdata',verbose_name='Loja')
    transportadora = models.ForeignKey(Transportadoras, verbose_name="Transportadora")
    peso_inicial = models.DecimalField('Peso Inicial', decimal_places=2, max_digits=10)
    peso_final = models.DecimalField('Peso Final', decimal_places=2, max_digits=10)
    cep_inicial = models.DecimalField('CEP Inicial', decimal_places=2, max_digits=10)
    cep_final = models.DecimalField('CEP Final', decimal_places=2, max_digits=10)
    tempo_entrega = models.PositiveSmallIntegerField('Tempo de Entrega')
    volume_maximo = models.DecimalField('Volume Máximo', decimal_places=2, max_digits=10)
    peso_cubado = models.DecimalField('Peso Cubado', decimal_places=2, max_digits=10)
    dt_inicio = models.DateField("Data Início de Vigência")
    dt_final = models.DateField("Data Final de Vigência")
    custo_acessorial = models.DecimalField('Custo Acessorial', decimal_places=3, max_digits=5)
    valor_minimo = models.DecimalField('Valor Mínimo', decimal_places=2, max_digits=10)
    fator_multiplicacao = models.DecimalField('Valor Fator de Multiplicação', decimal_places=2, max_digits=10)
    custos_adicionais = models.DecimalField('Custos Adicionais', decimal_places=2, max_digits=10)
    criado = models.DateTimeField("Criado em", auto_now_add=True)
    modificado = models.DateTimeField("Modificado em", auto_now=True)

    class Meta:
        verbose_name = 'Fretes'
        verbose_name_plural = 'Fretes'
        ordering = ['transportadora__nome_fantasia', 'peso_inicial']
        unique_together=[('loja','transportadora','dt_inicio'),]

    def __str__(self):
        return self.transportadora.nome_fantasia + ': ' + self.peso_inicial + '-' + self.peso_final






