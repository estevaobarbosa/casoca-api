from django.conf.urls import url


from Casoca.excel import views

# app_name = 'excel'

urlpatterns = [
    url(r'^$', views.model_form_upload, name='upload_file'),
]