from django import forms
from .models import Uploads
from cuser.middleware import CuserMiddleware

class UploadFileForm(forms.ModelForm):
    usuario = forms.CharField(widget=forms.HiddenInput(),
                              required=False)

    documento = forms.FileField(label='Selecione uma planilha excel',
                               widget=forms.FileInput(
                                   attrs={
                                       'class': 'form-control',
                                       'placeholder': 'Informe o arquivo com a planilha de fretes'
                                   }
                               ),
                               required=True,
                               )

    class Meta:
        model = Uploads
        fields = '__all__'

        widgets = {
            "loja": forms.Select(
                attrs={
                    'class': 'form-control',
                },
            ),
        }

    def clean_usuario(self):
        return CuserMiddleware.get_user()