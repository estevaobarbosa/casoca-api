from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
import locale


class Carga(object):

    def __init__(self):
        self.ConsistenciaOk = True
        self.fretes = []


    def processaPlanilha(self, loja, planilha):

        if self.carregaPlanilha(loja, planilha):
            resultado = self.consisteFretes()
            if self.ConsistenciaOk:
                resultado = self.gravaFretes()
        else:
            resultado = []
            resultado.append('Planilha incompatível com o padrão de frete')

        return resultado


    def carregaPlanilha(self, loja, planilha):

        contador = 0
        try:
            for line in planilha:

                line = str(line).replace('\n','').replace('[','').replace(']','').replace("'",'')
                values = line.split(',')

                contador += 1;
                values[0] = values[0].strip()
                if (values[0] != '' and contador > 1):
                    # Incializa Variáveis Locais
                    transportadora = ''
                    peso_inicial = 0.0
                    peso_final = 0.0
                    cep_inicial = 0
                    cep_final = 0
                    tempo_entrega = 0
                    volume_maximo = 0.0
                    peso_cubado = 0.0
                    dt_inicio = ''
                    dt_final = ''
                    custo_acessorial = 0.0
                    valor_minimo = 0.0
                    fator_multiplicacao = 0.0
                    custos_adicionais = 0.0

                    # Transportadora
                    values[0] = values[0].strip()
                    if values[0] != '':
                        transportadora = values[0]

                    # Peso Inicial
                    values[1] = values[1].strip()
                    if values[1] != '':
                        peso_inicial = float(values[1])

                    # Peso Final
                    values[2] = values[2].strip()
                    if values[2] != '':
                        peso_final = float(values[2])

                    # CEP Inicial
                    values[3] = values[3].strip()
                    if values[3] != '':
                        cep_inicial = int(values[3])

                    # CEP Final
                    values[4] = values[4].strip()
                    if values[4] != '':
                        cep_final = int(values[4])

                    # Tempo de Entrega
                    values[5] = values[5].strip()
                    if values[5] != '':
                        tempo_entrega = int(values[5])

                    # Volume Máximo
                    values[6] = values[6].strip()
                    if values[6] != '':
                        volume_maximo = float(values[6])

                    # Peso Cubado
                    values[7] = values[7].strip()
                    if values[7] != '':
                        peso_cubado = float(values[7])

                    # Data Início de Vigência
                    values[8] = values[8].strip()
                    if values[8] != '':
                        dt_inicio = values[8]

                    # Data Final de Vigência
                    values[9] = values[9].strip()
                    if values[9] != '':
                        dt_final = values[9]

                    # Custo Acessorial
                    values[10] = values[10].strip()
                    if values[10] != '':
                        custo_acessorial = float(values[10])

                    # Valor Mínimo
                    values[11] = values[11].strip()
                    if values[11] != '':
                        valor_minimo = float(values[11])

                    # Valor Fator de Multiplicação
                    values[12] = values[12].strip()
                    if values[12] != '':
                        fator_multiplicacao = float(values[12])

                    # Custos Adicionais
                    values[13] = values[13].strip()
                    if values[13] != '':
                        custos_adicionais = float(values[13])

                    transacao ={}
                    transacao['linha'] = contador
                    transacao['loja'] = loja
                    transacao['transportadora'] = transportadora
                    transacao['peso_inicial'] = peso_inicial
                    transacao['peso_final'] = peso_final
                    transacao['cep_inicial'] = cep_inicial
                    transacao['cep_final'] = cep_final
                    transacao['tempo_entrega'] = tempo_entrega
                    transacao['volume_maximo'] = volume_maximo
                    transacao['peso_cubado'] = peso_cubado
                    transacao['dt_inicio'] = dt_inicio
                    transacao['dt_final'] = dt_final
                    transacao['custo_acessorial'] = custo_acessorial
                    transacao['valor_minimo'] = valor_minimo
                    transacao['fator_multiplicacao'] = fator_multiplicacao
                    transacao['custos_adicionais'] = custos_adicionais
                    self.fretes.append(transacao)

        except Exception:
            return False

        return True

    def consisteFretes(self):

        for frete in self.fretes:
            pass

    def gravaFretes(self):

        for frete in self.fretes:
            pass