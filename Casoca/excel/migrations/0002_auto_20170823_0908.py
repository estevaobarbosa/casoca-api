# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-23 09:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('magento', '0002_marketplace_userdata'),
        ('excel', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploads',
            name='loja',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='magento.marketplace_userdata', verbose_name='Loja'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='uploads',
            name='documento',
            field=models.FileField(upload_to='planilhas/', verbose_name='Selecione uma planilha excel'),
        ),
    ]
