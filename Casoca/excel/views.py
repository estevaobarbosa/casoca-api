# -*- coding:utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist

from .loads import Carga
from .forms import UploadFileForm
from .models import Uploads




@login_required
def model_form_upload(request):
    arquivos = Uploads.objects.all().order_by('-uploaded_at')
    resultado = []
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            filehandle = request.FILES['documento']
            loja = request.POST.get('loja')
            file_name = str(filehandle)
            file_type = file_name.split(".")[-1]
            if file_type.lower() == 'xls' or file_type.lower() == 'xlsx':
                planilha = filehandle.get_sheet()
                form.save()
                carga = Carga()
                resultado = carga.carregaPlanilha(loja, planilha)
            else:
                resultado.append('Arquivo ' + file_name + ' não suportado, planilhas Excel são to tipo .xls ou .xlsx')
    else:
        form = UploadFileForm()
    return render(request, 'excel/upload.html', {
        'form': form,
        'mensagem':resultado,
        'arquivos':arquivos,
    })

