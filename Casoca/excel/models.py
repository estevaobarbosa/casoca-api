from django.db import models
from django.contrib.auth.models import User


class Uploads(models.Model):
    loja = models.ForeignKey('magento.marketplace_userdata',verbose_name='Loja')
    documento = models.FileField('Planilha Excel',upload_to='planilhas/')
    usuario = models.ForeignKey(User)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Upload'
        verbose_name_plural = 'Uploads'

    def __str__(self):
        return self.documento